//
//  BigService.swift
//  SwiftPackageApp
//
//  Created by Narendra Satpute on 14/01/21.
//

import Foundation
import Alamofire


public struct BigService {
    public static func doSomething() -> String {
       let str = "Hello Swift package manager!!!"
        print(str)
        return str
    }
    
    public static func doTesting() -> String {
       let str = "Test Swift package manager upadte!!!"
        print(str)
        return str
        
        
    }
}

